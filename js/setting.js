//menu header
$('.mobile-icon').on('click', function () {
	$(this).toggleClass("mobile-close");
	$(".nav-menu").toggleClass("active");
});


$(function () {
	objectFitImages('img');
});


jQuery(function ($) {
	$('.news-list .item .note').matchHeight();
	$('.news-list .item .img').matchHeight();
	$('.news-list .item .note02').matchHeight();
	$('.news-list .item .txt-basic').matchHeight();
	$('.tutorial').matchHeight();
});

$('.info-block .item .btn-type01').click(function () {
	$('.purchase-box').addClass('on');
	$('html').addClass('no-scroll');
});
$('.purchase-box').click(function () {
	$(this).removeClass('on');
	$('html').removeClass('no-scroll');
});
$('.purchase-box_content').click(function (e) {
	e.stopPropagation();
});

